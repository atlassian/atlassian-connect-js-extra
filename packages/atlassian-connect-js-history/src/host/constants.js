export const STATE_AP_KEY = '_AP';

export const QUERY_KEY_PREFIX = 'ac';
export const QUERY_KEY_DELIMITER = '.';
export const ANCHOR_PREFIX = "!";

export const HISTORY_POPSTATE = 'history_popstate';
export const HISTORY_PUSHSTATE = 'history_pushstate';
export const HISTORY_REPLACESTATE = 'history_replacestate';
export const HISTORY_CHANGESTATE = 'history_changestate';
