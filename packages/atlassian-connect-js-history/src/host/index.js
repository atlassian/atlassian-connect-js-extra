/**
* The History API allows your add-on to manipulate the current page URL for use in navigation. When using
* the history module only the page anchor is modified and not the entire window location.
*
* Note: This is only enabled for page modules (Admin page, General page, Configure page, User profile page).
* It cannot be used if the page module is launched as a dialog.
* ### Example ###
* ```
* // Register a function to run when state is changed.
* // You should use this to update your UI to show the state.
* AP.history.popState(function(e){
*     alert("The URL has changed from: " + e.oldURL + "to: " + e.newURL);
* });
*
* // Adds a new entry to the history and changes the url in the browser.
* AP.history.pushState("page2");
*
* // Changes the URL back and invokes any registered popState callbacks.
* AP.history.back();
* ```
* @module History
*/

import {
  HISTORY_POPSTATE,
  HISTORY_PUSHSTATE,
  HISTORY_REPLACESTATE,
  HISTORY_CHANGESTATE
} from './constants';
import {
  Addon,
  Addons,
  Change,
  Current,
  State,
  Route
} from './models';
import {
  values,
  wrapState,
  unwrapState,
  createEvent,
  callConnectHost,
  log
} from './utils';


function broadcastEvents (eventName, evt) {
  callConnectHost(function (ch) {
    const extensions = values(ch.getExtensions());
    extensions.forEach(function (ext) {
      const addonKey = ext.extension.addon_key;
      const current = new Current({
        url: window.location.href,
        state: evt.state
      });
      const route = new Route(current);
      ch.broadcastEvent(eventName, {
        addon_key: addonKey
      }, route.render(addonKey));
    });
  });
}

var registeredWindowListeners = false;

function registerWindowListeners () {
  if (registeredWindowListeners) return;

  window.addEventListener('popstate', function (e) {
    const evt = createEvent('ap_popstate');
    evt.state = unwrapState(e.state);
    window.dispatchEvent(evt);
  });

  window.addEventListener('ap_popstate', function (e) {
    const evt = createEvent('ap_changestate');
    evt.state = e.state;
    window.dispatchEvent(evt);
    broadcastEvents(HISTORY_POPSTATE, e);
  });

  window.addEventListener('ap_pushstate', function (e) {
    const evt = createEvent('ap_changestate');
    evt.state = e.state;
    window.dispatchEvent(evt);
    broadcastEvents(HISTORY_PUSHSTATE, e);
  });

  window.addEventListener('ap_replacestate', function (e) {
    const evt = createEvent('ap_changestate');
    evt.state = e.state;
    window.dispatchEvent(evt);
    broadcastEvents(HISTORY_REPLACESTATE, e);
  });

  window.addEventListener('ap_changestate', function (e) {
    broadcastEvents(HISTORY_CHANGESTATE, e);
  });

  registeredWindowListeners = true;
}

function _pushState (route) {
  const renderedState = route.state.render();
  const renderedURL = route.url.render();
  window.history.pushState(wrapState(renderedState), renderedURL.title, renderedURL.url);
}

function _replaceState (route) {
  const renderedState = route.state.render();
  const renderedURL = route.url.render();
  window.history.replaceState(wrapState(renderedState), renderedURL.title, renderedURL.url);
}

export default {
  _registerWindowListeners: function () {
    registerWindowListeners();
  },
  /**
  * Goes back one step in the joint session history. Will invoke the popState callback.
  * @noDemo
  * @example
  * AP.history.back(); // go back by 1 entry in the browser history.
  */
  back: function (callback) {
    callback = arguments[arguments.length - 1];
    this.go(-1, callback);
  },
  /**
  * Goes forward one step in the joint session history. Will invoke the popState callback.
  * @noDemo
  * @example
  * AP.history.forward(); // go forward by 1 entry in the browser history.
  */
  forward: function (callback) {
    callback = arguments[arguments.length - 1];
    this.go(1, callback);
  },
  /**
  * Moves the page history back or forward the specified number of steps. A zero delta will reload the current page.
  * If the delta is out of range, does nothing. This call invoke the popState callback.
  * @param {Integer} delta - Number of places to move in the history
  * @noDemo
  * @example
  * AP.history.go(-2); // go back by 2 entries in the browser history.
  */
  go: function (delta) {
    const callback = arguments[arguments.length - 1];
    if(callback._context.extension.options.isFullPage) {
      window.history.go(delta);
    } else {
      log("History is only available to page modules");
    }
  },
  /**
  * Retrieves the current state of the history stack and returns the value. The returned value is the same as what was set with the pushState method.
  * @param {String} type - Type of requested value (optional). Valid values are undefined, "hash" and "all".
  * @param {Function} callback - Asynchronous callback (optional) if retrieving state during page load.
  * @return {String} The current url anchor
  * @noDemo
  * @example
  * AP.history.pushState("page5");
  * AP.history.getState(); // returns "page5";
  *
  * // asynchronous
  * AP.history.getState('hash', function(hash) {
  *   console.log("hash: ", hash);
  * });
  */
  getState: function (type, callback) {
    callback = arguments[arguments.length - 1];
    const addonKey = callback._context.extension.addon_key;
    const current = new Current({
      url: window.location.href,
      state: unwrapState(window.history.state)
    });
    const route = new Route(current);
    const rendered = route.render(addonKey);
    type = type
      ? (typeof type === 'string' ? type : 'hash')
      : 'hash';
    if (type === 'hash') {
      callback(rendered.hash);
    } else if (type === 'all') {
      callback(rendered);
    } else {
      throw new Error('invalid type for getState');
    }
  },

  /**
  * Updates the location's anchor with the specified value and pushes the given data onto the session history.
  * Will invoke the popState callback.
  * @param {String | Object} newState - URL or state object to add to history
  */
  pushState: function (newState, callback) {
    callback = arguments[arguments.length - 1];
    const addonKey = callback._context.extension.addon_key;
    const current = new Current({
      url: window.location.href,
      state: unwrapState(window.history.state)
    });
    const currentRoute = new Route(current);
    const change = new Change(newState, addonKey);
    const changeRoute = new Route(change);
    if (callback._context.extension.options.isFullPage) {
      if (changeRoute.url.isURLEqual(currentRoute.url)) {
        currentRoute.merge(changeRoute);
        _replaceState(currentRoute);
      } else {
        if (changeRoute.url.isPathnameEqual(currentRoute.url)) {
          currentRoute.merge(changeRoute);
          _pushState(currentRoute);
        } else {
          _pushState(changeRoute);
        }
      }
      const event = createEvent('ap_pushstate');
      event.state = unwrapState(window.history.state);
      window.dispatchEvent(event);
    } else {
      log('History is only available to page modules');
    }
  },
  /**
  * Updates the current entry in the session history. Updates the location's anchor with the specified value but does not change the session history.
  * Will invoke the popState callback.
  * @param {String | Object} newState - URL or state object to update current history value with
  */
  replaceState: function (newState, callback) {
    callback = arguments[arguments.length - 1];
    const addonKey = callback._context.extension.addon_key;
    const current = new Current({
      url: window.location.href,
      state: unwrapState(window.history.state)
    });
    const currentRoute = new Route(current);
    const change = new Change(newState, addonKey);
    const changeRoute = new Route(change);
    if (callback._context.extension.options.isFullPage) {
      if (changeRoute.url.isURLEqual(currentRoute.url)) {
        currentRoute.merge(changeRoute);
        _replaceState(currentRoute);
      } else {
        if (changeRoute.url.isPathnameEqual(currentRoute.url)) {
          currentRoute.merge(changeRoute);
          _replaceState(currentRoute);
        } else {
          _replaceState(changeRoute);
        }
      }
      const event = createEvent('ap_replacestate');
      event.state = unwrapState(window.history.state);
      window.dispatchEvent(event);
    } else {
      log('History is only available to page modules');
    }
  }
};
