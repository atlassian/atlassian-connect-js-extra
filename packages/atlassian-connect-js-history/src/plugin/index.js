(function () {
  if(!AP._hostModules.history) {
    return;
  }

  const HISTORY_POPSTATE = 'history_popstate';
  const HISTORY_PUSHSTATE = 'history_pushstate';
  const HISTORY_REPLACESTATE = 'history_replacestate';
  const HISTORY_CHANGESTATE = 'history_changestate';

  function find (arr, iterator) {
    for (var i = 0, l = arr.length; i < l; i += 1) {
      if (iterator(arr[i])) {
        return arr[i];
      }
    }
  }

  function Subscription (opts) {
    var self = this;
    var typeToEventName = {
      'pop': HISTORY_POPSTATE,
      'push': HISTORY_PUSHSTATE,
      'replace': HISTORY_REPLACESTATE,
      'change': HISTORY_CHANGESTATE
    };
    if (!opts.listener) {
      throw new Error('missing listener for subscription');
    }
    if (!opts.type) {
      throw new Error('missing type for subscription');
    }
    if (!typeToEventName[opts.type]) {
      throw new Error('invalid type for subscription');
    }
    this.listener = opts.listener;
    this.type = opts.type;
    this.eventName = typeToEventName[this.type];
    this.lastValue = opts.lastValue;
    this.attribute = opts.attribute;
    this.handler = function (state) {
      if (typeof self.attribute === 'string') {
        // only on attribute change do we trigger listener
        if (self.lastValue !== state[self.attribute]) {
          state.newURL = state[self.attribute];
          state.oldURL = self.lastValue;
          self.lastValue = state[self.attribute];
          return self.listener(state);
        }
      } else {
        return self.listener(state);
      }
    };
  }

  function Subscriptions (opts) {
    opts.models = opts.models || [];
    if (!(opts.models instanceof Array)) {
      opts.models = [opts.models];
    }
    opts.models = opts.models.map(function (model) {
      if (model instanceof Subscription) {
        return model;
      } else {
        return new Subscription(model);
      }
    });
    this.models = opts.models;
    return this;
  }

  Subscriptions.prototype.add = function (subscription) {
    this.models.push(subscription);
  };

  Subscriptions.prototype.remove = function (subscription) {
    this.models.splice(this.models.indexOf(subscription), 1);
  };

  Subscriptions.prototype.find = function (type, listener) {
    return find(this.models, function (subscription) {
      return (subscription.type === type) && (subscription.listener === listener);
    });
  };

  var subscriptions = new Subscriptions({
    models: []
  });

  function registerListener (eventName) {
    return function (event) {
      var subs = subscriptions.models.filter(function (sub) {
        return sub.eventName === eventName;
      });
      subs.forEach(function (sub) {
        sub.handler(event);
      });
    };
  }

  AP.register({
    [HISTORY_POPSTATE]: registerListener(HISTORY_POPSTATE),
    [HISTORY_PUSHSTATE]: registerListener(HISTORY_PUSHSTATE),
    [HISTORY_REPLACESTATE]: registerListener(HISTORY_REPLACESTATE),
    [HISTORY_CHANGESTATE]: registerListener(HISTORY_CHANGESTATE)
  });

  /**
   * Register a function to be executed on state change scoped to _hash.
   * @name popState
   * @method   
   * @param {Function} callback - Callback method to be subscribed for state change scoped to _hash.
   */
  function popState (listener) {
    AP.history._registerWindowListeners();
    var subscription = subscriptions.find('change', listener);
    if (subscription) return;
    subscription = new Subscription({
      type: 'change',
      attribute: 'hash',
      listener: listener
    });
    subscriptions.add(subscription);
  }
  AP._hostModules.history.popState = popState;
  AP.history.popState = popState;

  /**
   * Register a function to be executed on state change.
   * @name subscribeState
   * @method
   * @param {Function} callback - Callback method to be subscribed for state change.
   */
  function subscribeState (type, listener) {
    AP.history._registerWindowListeners();
    var subscription = subscriptions.find(type, listener);
    if (subscription) return;
    var subscription = new Subscription({
      type: type,
      listener: listener
    });
    subscriptions.add(subscription);
  }
  AP._hostModules.history.subscribeState = subscribeState;
  AP.history.subscribeState = subscribeState;

  /**
   * Unregister a function to be executed on state change.
   * @name unsubscribeState
   * @method
   * @param {Function} callback - Callback method to be unsubscribed for state change.
   */
  function unsubscribeState (type, listener) {
    var subscription = subscriptions.find(type, listener);
    if (!subscription) return;
    subscriptions.remove(subscription);
  }
  AP._hostModules.history.unsubscribeState = unsubscribeState;
  AP.history.unsubscribeState = unsubscribeState;

  // support for deprecated sync history.getState() syntax.
  // we need to remove the history module section from atlassian-connect/jsapi-v5/src/main/resources/v5/js/iframe/plugin/p2compat.js
  var _state = null;
  if(AP._data.options && AP._data.options.history && AP._data.options.history.state){
    _state = {
      hash: AP._data.options.history.state
    };
  }

  AP.history.getState('all', function (s) {
    _state = s;
  });
  AP.history.subscribeState('change', function (s) {
    _state = s;
  });
  var original_getState = AP._hostModules.history.getState.bind({});
  AP.history.getState = AP._hostModules.history.getState = function (type, callback) {
    callback = arguments[arguments.length - 1];
    if (typeof callback === 'function') {
      if (typeof type === 'string') {
        return original_getState(type, callback);
      } else {
        return original_getState(callback);
      }
    } else {
      type = type || 'hash';
      if (type === 'hash') {
        return _state && _state.hash ? _state.hash : '';
      } else if (type === 'all') {
        return _state;
      } else {
        throw new Error('invalid type for getState');
      }
    }
  };

}());

