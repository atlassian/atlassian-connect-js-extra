(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.connectHostCookie = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/**
* Enables apps to store, retrieve, and delete client side data.
* The data is stored in the Atlassian product's localStorage, not the app's localStorage.
* This data cannot be seen by other apps.
*
* This module is named "Cookie" as it originally used cookies to store data.
* It now uses localStorage, but the name has been kept for backwards compatibility.
* @exports Cookie
*/

// taken in part from https://bitbucket.org/atlassian/aui/src/master/packages/core/src/js/aui/cookie.js
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var COOKIE_NAME = 'AJS.conglomerate.cookie';
var UNESCAPE_COOKIE_REGEX = /(\\|^"|"$)/g;
var CONSECUTIVE_PIPE_CHARS_REGEX = /\|\|+/g;
var ANY_QUOTE_REGEX = /"/g;
var REGEX_SPECIAL_CHARS = /[.*+?|^$()[\]{\\]/g;
var PIPE_PERCENT_ENCODED_UPPER = encodeURIComponent('|'); // %7C
var PIPE_PERCENT_ENCODED_LOWER = encodeURIComponent('|').toLowerCase(); // %7c
var PIPE_REPLACEMENT_STR = '**PIPE**';
var PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER = '**PIPE_PERCENT_ENCODED_UPPER**';
var PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER = '**PIPE_PERCENT_ENCODED_LOWER**';
var PERCENT_REPLACEMENT_STR = '**PERCENT**';
var EQUALS_REPLACEMENT_STR = '**EQUALS**';

function encodePipesAndPercents(str) {
  if (typeof str !== 'string') {
    return str;
  }
  if (str.indexOf(PIPE_REPLACEMENT_STR) !== -1 || str.indexOf(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER) !== -1 || str.indexOf(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER) !== -1) {
    throw new Error('Cannot use cookies that contain ' + PIPE_REPLACEMENT_STR + ' or ' + PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER + PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER);
  }
  if (str.indexOf(PERCENT_REPLACEMENT_STR) !== -1) {
    throw new Error('Cannot use cookies that contain ' + PERCENT_REPLACEMENT_STR);
  }
  if (str.indexOf(EQUALS_REPLACEMENT_STR) !== -1) {
    throw new Error('Cannot use cookies that contain ' + EQUALS_REPLACEMENT_STR);
  }

  str = str.replace(new RegExp(regexEscape('|'), 'g'), PIPE_REPLACEMENT_STR);
  str = str.replace(new RegExp(PIPE_PERCENT_ENCODED_UPPER, 'g'), PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER);
  str = str.replace(new RegExp(PIPE_PERCENT_ENCODED_LOWER, 'g'), PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER);
  str = str.replace(new RegExp('%', 'g'), PERCENT_REPLACEMENT_STR);
  str = str.replace(new RegExp('=', 'g'), EQUALS_REPLACEMENT_STR);
  return str;
}

function decodePipesAndPercents(str) {
  if (typeof str !== 'string') {
    return str;
  }
  str = str.replace(new RegExp(regexEscape(PIPE_REPLACEMENT_STR), 'g'), '|');
  str = str.replace(new RegExp(regexEscape(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_UPPER), 'g'), PIPE_PERCENT_ENCODED_UPPER);
  str = str.replace(new RegExp(regexEscape(PIPE_PERCENT_ENCODED_REPLACEMENT_STR_LOWER), 'g'), PIPE_PERCENT_ENCODED_LOWER);
  str = str.replace(new RegExp(regexEscape(PERCENT_REPLACEMENT_STR), 'g'), '%');
  str = str.replace(new RegExp(regexEscape(EQUALS_REPLACEMENT_STR), 'g'), '=');
  return str;
}

function regexEscape(str) {
  return str.replace(REGEX_SPECIAL_CHARS, '\\$&');
}

function getValueFromConglomerate(name, cookieValue) {
  // A null cookieValue is just the first time through so create it.
  cookieValue = cookieValue || '';
  var reg = new RegExp(regexEscape(name) + '=([^|]+)');
  var res = cookieValue.match(reg);
  return res && res[1];
}

// Either append or replace the value in the cookie string/
function addOrAppendToValue(name, value, cookieValue) {
  // A cookie name follows after any amount of white space mixed with any amount of '|' characters.
  // A cookie value is preceded by '=', then anything except for '|'.
  var reg = new RegExp('(\\s|\\|)*\\b' + regexEscape(name) + '=[^|]*[|]*');

  cookieValue = cookieValue || '';
  cookieValue = cookieValue.replace(reg, '|');

  if (value !== '') {
    var pair = name + '=' + value;
    if (cookieValue.length + pair.length < 4020) {
      cookieValue += '|' + pair;
    }
  }

  return cookieValue.replace(CONSECUTIVE_PIPE_CHARS_REGEX, '|');
}

function unescapeCookieValue(name) {
  return name.replace(UNESCAPE_COOKIE_REGEX, '');
}

function getCookieValue(name) {
  var reg = new RegExp('\\b' + regexEscape(name) + '=((?:[^\\\\;]+|\\\\.)*)(?:;|$)');
  var res = document.cookie.match(reg);
  return res && unescapeCookieValue(res[1]);
}

function saveCookie(name, value, days) {
  var ex = '';
  var d;
  var quotedValue = '"' + value.replace(ANY_QUOTE_REGEX, '\\"') + '"';

  if (days) {
    d = new Date();
    d.setTime(+d + days * 24 * 60 * 60 * 1000);
    ex = '; expires=' + d.toGMTString();
  }

  document.cookie = name + '=' + quotedValue + ex + ';path=/';
}

function save(name, value, expires) {
  var cookieValue = getCookieValue(COOKIE_NAME);
  cookieValue = addOrAppendToValue(name, value, cookieValue);
  saveCookie(COOKIE_NAME, cookieValue, expires || 365);
}

function _read(name, defaultValue) {
  var cookieValue = getCookieValue(COOKIE_NAME);
  var value = getValueFromConglomerate(name, cookieValue);
  if (value != null) {
    return value;
  }
  return defaultValue;
}

function _erase(name) {
  save(name, '');
}

function validateAddonKeyName(addonKey, name) {
  if (!addonKey || addonKey.length === 0) {
    throw new Error('addon key must be defined on cookies');
  }

  if (!name || name.length === 0) {
    throw new Error('Name must be defined');
  }
}

function prefixCookie(addonKey, name) {
  validateAddonKeyName(addonKey, name);
  return addonKey + '$$' + name;
}

function prefixLocalStorage(addonKey, name) {
  validateAddonKeyName(addonKey, name);
  return 'connect.cookie:' + addonKey + '$$' + name;
}

function addonKeyFromCallback(callback) {
  if (callback && callback._context) {
    return callback._context.extension.addon_key;
  } else {
    throw new Error('addon key not found in callback');
  }
}

function saveLocalStorage(addonKey, name, value, expireDays) {
  var date = new Date();
  date.setTime(date.getTime() + (expireDays || 365) * 24 * 60 * 60 * 1000);

  localStorage.setItem(prefixLocalStorage(addonKey, name), JSON.stringify({
    expiry: date.getTime(),
    value: value
  }));
}

function readLocalStorage(addonKey, name) {
  var itemStr = localStorage.getItem(prefixLocalStorage(addonKey, name));
  if (itemStr === null) {
    return;
  }

  var item = JSON.parse(itemStr);
  if (item.expiry && new Date().getTime() > item.expiry) {
    localStorage.removeItem(name);
    return;
  }

  var value = item.value;
  // Original API always returned string, so we need to match it
  return typeof value === 'string' ? value : JSON.stringify(value);
}

exports['default'] = {
  /**
  * Save a value to localStorage.
  * @param name {String}    The name to store the data against.
  * @param value {String}   The value to store.
  * @param expires {Number} Optional, the number of days before expiry, defaults to 365.
  * @noDemo
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  */
  save: function save(name, value, expires) {
    var callback = arguments[arguments.length - 1];
    var addonKey = addonKeyFromCallback(callback);
    if (callback._context) {
      saveLocalStorage(addonKey, name, value, expires);
    }
  },
  /**
  * Get a value from localStorage (falls back to cookie for legacy support).
  * @param name {String} The name to store the data against.
  * @param callback {Function} The callback to pass the data to.
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  */
  read: function read(name, callback) {
    callback = arguments[arguments.length - 1];

    var lsValue = readLocalStorage(addonKeyFromCallback(callback), name);
    // Only use value from localStorage if it's set (fallback to cookie value)
    if (lsValue !== undefined) {
      callback(lsValue);
      return lsValue; // for testing
    }

    var cookieName = prefixCookie(addonKeyFromCallback(callback), encodePipesAndPercents(name));
    var value = decodePipesAndPercents(_read(cookieName));
    callback(value);
    return value; // for testing
  },
  /**
  * Deletes a value from localStorage.
  * @param name {String} The name of the data to remove.
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  * AP.cookie.erase('my_cookie');
  */
  erase: function erase(name) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), encodePipesAndPercents(name));
    _erase(cookieName);

    localStorage.removeItem(prefixLocalStorage(addonKeyFromCallback(callback), name));
  }
};
module.exports = exports['default'];
},{}]},{},[1])(1)
});
