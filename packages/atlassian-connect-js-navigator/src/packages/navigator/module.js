/**
 * The Navigator API allows your add-on to change the current page using JavaScript.
 *
 * @module Navigator
 */

/**
 * @class Navigator~target~Confluence
 * @property {String}        contentview        The view page for pages, blogs and custom content. Takes a `contentId` to identify the content.
 * @property {String}        contentedit        The edit page for pages, blogs and custom content. Takes a `contentId` to identify the content.
 * @property {String}        spaceview          The space view page. Takes a `spaceKey` to identify the space.
 * @property {String}        spacetools         The space tools page. Takes a `spaceKey` to identify the space.
 * @property {String}        dashboard          The dashboard of Confluence.
 * @property {String}        userProfile        The profile page for a specific user. Takes a `username` or `userAccountId` to identify the user.
 * @property {String}        addonModule        The module page within a specific add-on. Takes an `addonKey` and a `moduleKey` to identify the correct module.
 * @property {String}        contentlist        The list/collector page for pages, blogs and custom content contained in a space. Takes a `spaceKey` and a `contentType` to identify the content type.
 */

/**
 * @class Navigator~target~Jira
 * @property {String}        dashboard          A specific dashboard in Jira. Takes a `dashboardId` to identify the dashboard.
 * @property {String}        issue              A specific Issue in Jira. Takes an `issueKey` to identify the issue.
 * @property {String}        addonModule        The module page within a specific add-on. Takes an `addonKey` and a `moduleKey` to identify the correct module.
 * @property {String}        userProfile        The profile page for a Jira User. Takes a `username` or `userAccountId` to identify the user.
 * @property {String}        projectAdminSummary    The admin details of a specific Jira Project. Takes a `projectKey` to identify the project. Only accessible to administrators.
 * @property {String}        projectAdminTabPanel   The admin panel definted by a connect addon. Takes an `addonKey`, `adminPageKey`, `projectKey` and `projectId`. Only accessible to administrators.
 */

/**
 * @class Navigator~context
 * @property {String}        contentId          Identifies a piece of content. Required for the `contentView` target.
 * @property {String}        contentType        Identifies the type of content. Can be either `page` or `blogpost`. Required for the `contentEdit` target.
 * @property {String}        spaceKey           Identifies a space. Required for the `spaceView` and `spaceTools` targets.
 * @property {String}        username           Identifies a user. One of this or `userAccountId` required for the `userProfile` target.
 * @property {String}        userAccountId      Identifies a user. One of this or `username` required for the `userProfile` target.
 * @property {String}        addonKey           Identifies a connect add-on. Required for the `addonModule` and `projectAdminTabPanel` targets.
 * @property {String}        moduleKey          Identifies a connect add-on module. Required for the `addonModule` target.
 * @property {String}        dashboardId        Identifies a Jira dashboard. Required for the `dashboard` target in Jira.
 * @property {String}        projectKey         Identifies a Jira project. Required for the `projectSummary`, `projectAdminSummary` and `projectAdminTabPanel` targets.
 * @property {String}        issueKey           Identifies a Jira issue. Required for the `issue` target.
 * @property {String}        adminPageKey       Identifies a Jira Project Admin Tab Panels module key. Required for the `projectAdminTabPanel` target.
 * @property {String}        projectId          Identifies a Jira Project by its ID number. Required for the `projectAdminTabPanel` target.
 * @property {String}        customData         Contains parameters that will be added as query parameters to the product url with "ac." prepended. Used only in `addonModule` target.  See <a href="../latest/concepts/context-parameters.html#add-on-specific-context-parameters">Add-on specific context parameters</a> for more info.
 * @property {String}        versionOverride    Identifies a version of a piece of content in Confluence. This parameter is optional, and only applies to the `contentView` target, allowing navigation to a specific version.
 * @property {String}        embeddedContentRender    Identifies the mode for rendering embedded content in Confluence, such as attachments embedded in a page. This only applies to the `contentView` target. Valid values are `current` (render the embedded content using the latest version) and `version-at-save` (render the embedded content using the version at the time the content was saved). This parameter is optional and defaults to `current`.
 */
import navigatorUtils from './utils';
import navigatorRoutes from './routes';
import navigatorContext from './context';
export default {
    /**
     *
     * Returns the context of the current page within the host application.
     *
     * This method will provide a context object to the passed in callback.  This context object
     * will contain information about the page currently open in the host application.
     *
     * The object will contain a target, which can be used when calling the `go` method, and a context map containing in formation about the opened page.
     *
     * Currently this method supports two contexts in Confluence only:
     *
     * <strong>contentview</strong> - The host application is currently viewing a page, blog post or other content.
     *
     * <strong>contentedit</strong> - the host application is currently editing a page, blog post or other content.
     *
     * @name getLocation
     * @method
     * @memberof module:Navigator
     * @param {Function} callback function (location) {...}
     * @noDemo
     * @example
     * AP.navigator.getLocation(function (location) {
     *   console.log(location)
     *   // location will be:
     *   // {
     *   //   "target": "contentview",
     *   //   "context": {
     *   //     "contentId": "1234",
     *   //     "contentType": "page",
     *   //     "spaceKey": "DS"
     *   //   }
     *   // }
     * });
     */
    getLocation: function(callback){
        var cf = navigatorContext.getContextFunction();
        if(typeof cf !== 'function'){
            console.error('no context function defined');
        } else {
            var context = cf();
            if($.type(context) === "object"){
                callback(context);
            } else {
                console.error('navigator context callback did not return an object');
            }
        }
    },
    /**
     * Navigates the user from the current page to the specified page. This call navigates the host product, not the iframe content.
     *
     * Requires a target location and the corresponding context. Navigating by passing a concrete url is currently unsupported.
     * @name go
     * @method
     * @memberof module:Navigator
     * @param {Navigator~target~Jira|Navigator~target~Confluence} target The type of page to navigate to.
     * @param {Navigator~context} context Specific information that identifies the page to navigate to.
     * @noDemo
     * @example
     * // To navigate to the view page for a piece of content in Confluence:
     * AP.navigator.go('contentview', {contentId: '12345'});
     *
     * // To navigate to the view page for version 2 of a piece of content in Confluence:
     * AP.navigator.go('contentview', {contentId: '12345', versionOverride: 2});
     *
     * // To navigate to the edit page for a piece of content in Confluence:
     * AP.navigator.go('contentedit', {contentId: '12345'});
     *
     * // To navigate to the issue view page of an issue in Jira:
     * AP.navigator.go('issue', {
     *   issueKey: 'TEST-1'
     * });
     */
    go: function(target, context, callback){
        target = target.toLowerCase();
        context = context || {};
        if (!context.addonKey){
            context.addonKey = callback._context.extension.addon_key;
        }
        if(!navigatorUtils.isApiEnabled()){
            console.error('connect navigation api not yet implemented for this product');
            return;
        }
        var productRoutes = navigatorRoutes.getRoutes();
        if(navigatorRoutes.hasRoutes() && target in productRoutes){
            if (typeof productRoutes[target] === "function") {
                productRoutes[target](context, navigatorUtils.goToUrl);
            } else {
                navigatorUtils.goToUrl(navigatorUtils.buildUrl(productRoutes[target], context));
            }
        } else {
            console.error("The URL target " + target + " is not available. Valid targets are: " + Object.keys(productRoutes).toString());
        }
    },
    /**
     * Triggers a reload of the parent page.
     * @name reload
     * @method
     * @memberof module:Navigator
     * @noDemo
     * @example
     * AP.navigator.reload();
     */
    reload: function() {
        window.location.reload();
    }
};
