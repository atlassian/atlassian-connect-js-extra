function createPathSegmentFromContext(segments) {
    const path = segments.map(encodeURIComponent).join('/');

    // Sanitize to prevent path traversal
    const url = new URL(path, window.location.origin);

    // Strip off the initial slash added by URL (slashes added by user would be URL encoded anyways)
    return url.pathname.slice(1);
}

function createQueryParamFromContext(params) {
    return Object.keys(params).map(function (prop) {
        return ['ac.' + prop, params[prop]].map(encodeURIComponent).join('=');
    }).join('&');
}

function getBooleanFeatureFlag(flagName, defaultValue) {
    if (window.connectHost && window.connectHost.getBooleanFeatureFlag) {
        return window.connectHost.getBooleanFeatureFlag(flagName);
    }
    return defaultValue;
}

class NavigatorRoutes {
    constructor(){
        this.routes = {};
    }

    hasRoutes() {
        return (this.routes && Object.getOwnPropertyNames(this.routes).length !== 0);
    }

    addRoutes (newRoutes) {
        this.routes = {...this.routes, ...newRoutes};
    }

    getRoutes() {
        return this.routes;
    }
}

let navigatorRoutes = new NavigatorRoutes();
let defaultRoutes = {
    "addonmodule": function (context, callback) {
        var addonKey = context && context['addonKey'];
        var moduleKey = context && context['moduleKey'];

        if(!addonKey) {
            throw new Error ('Missing addonKey parameter in the context.');
        }
        if(!moduleKey) {
            throw new Error ('Missing moduleKey parameter in the context.');
        }

        var addonPath = createPathSegmentFromContext([addonKey, moduleKey]);
        var url = AJS.contextPath() + '/plugins/servlet/ac/' + addonPath;

        if (context['context'] != undefined) {
            console.warn("DEPRECATED API - The context field has been deprecated in favor of customData.");
            connectHost.trackDeprecatedMethodUsed("AP.navigate-context", {
                addon_key: addonKey,
                moduleKey: moduleKey
            });
        }

        var paramsToPlaceInURL = AJS.$.extend(
            {},
            context['context'] || {},
            context['customData'] || {});

        var queryParameters = createQueryParamFromContext(paramsToPlaceInURL);
        url = (queryParameters != '') ? url + '?' + queryParameters : url;


        callback.apply(this, [url]);
    }
};
navigatorRoutes.addRoutes(defaultRoutes);

export default navigatorRoutes;
