import NavigatorRoutes from '../../src/packages/navigator/routes';

describe('Navigator Routes', function(){
    const callbackStub = jasmine.createSpy();
    const originalContextPath = AJS.contextPath;


    beforeEach(function () {
        callbackStub.calls.reset();
        AJS.contextPath = jasmine.createSpy('AJS.contextPath').and.returnValue('http://localhost:8080/wiki');
    });

    afterEach(function() {
        AJS.contextPath = originalContextPath;
    });

    it('should have one route by default', function () {
        var routes = NavigatorRoutes.getRoutes();
        expect(Object.getOwnPropertyNames(routes).length).toEqual(1);
        expect(NavigatorRoutes.hasRoutes()).toBe(true);
    });

    it('should not do anything when context is not specified', function () {
        var routes = NavigatorRoutes.getRoutes();

        expect(function() {routes["addonmodule"](null, callbackStub); }).toThrow(new Error("Missing addonKey parameter in the context."));
        expect(callbackStub.calls.count()).toEqual(0);
    });

    it('should not do anything when addon key and module key are not specified', function () {
        var context = {};
        var routes = NavigatorRoutes.getRoutes();

        expect(function() {routes["addonmodule"](context, callbackStub); }).toThrow(new Error("Missing addonKey parameter in the context."));
        expect(callbackStub.calls.count()).toEqual(0);
    });

    it('should not do anything when module key is not specified', function () {
        var context = {
            'addonKey': 'myaddon'
        };
        var routes = NavigatorRoutes.getRoutes();

        expect(function() {routes["addonmodule"](context, callbackStub); }).toThrow(new Error("Missing moduleKey parameter in the context."));
        expect(callbackStub.calls.count()).toEqual(0);
    });

    it('should apply callback with a correct url when addon key and moduleKey are available', function () {
        var context = {
            'addonKey': 'myaddon',
            'moduleKey': 'mymodule'
        };
        var routes = NavigatorRoutes.getRoutes();

        routes["addonmodule"](context, callbackStub);
        expect(callbackStub.calls.argsFor(0)).toEqual(['http://localhost:8080/wiki/plugins/servlet/ac/myaddon/mymodule']);
    });

    it('should sanitize url traversal', function () {
        var context = {
            'addonKey': '..',
            'moduleKey': 'ac-redirect'
        };
        var routes = NavigatorRoutes.getRoutes();

        routes["addonmodule"](context, callbackStub);
        expect(callbackStub.calls.argsFor(0)).toEqual(['http://localhost:8080/wiki/plugins/servlet/ac/ac-redirect']);
    });

    it('should encode slashes in keys', function () {
        var context = {
            'addonKey': '/myaddon',
            'moduleKey': 'mymodule'
        };
        var routes = NavigatorRoutes.getRoutes();

        routes["addonmodule"](context, callbackStub);
        expect(callbackStub.calls.argsFor(0)).toEqual(['http://localhost:8080/wiki/plugins/servlet/ac/%2Fmyaddon/mymodule']);
    });

    it('should apply callback with a correct url when addon key and moduleKey and a param are available', function () {
        var context = {
            'addonKey': 'myaddon',
            'moduleKey': 'mymodule',
            'customData': {
                'param1': 'paramValue1'
            }
        };
        var routes = NavigatorRoutes.getRoutes();

        routes["addonmodule"](context, callbackStub);
        expect(callbackStub.calls.argsFor(0)).toEqual(['http://localhost:8080/wiki/plugins/servlet/ac/myaddon/mymodule?ac.param1=paramValue1']);
    });

    it('should apply callback with a correct url when addon key and moduleKey and many params are available', function () {
        var context = {
            'addonKey': 'myaddon',
            'moduleKey': 'mymodule',
            'customData': {
                'param1': 'paramValue1',
                'param2': 'paramValue2'
            }
        };
        var routes = NavigatorRoutes.getRoutes();

        routes["addonmodule"](context, callbackStub);
        expect(callbackStub.calls.argsFor(0)).toEqual(['http://localhost:8080/wiki/plugins/servlet/ac/myaddon/mymodule?ac.param1=paramValue1&ac.param2=paramValue2']);
    });
});
