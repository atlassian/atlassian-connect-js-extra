import uritemplate from 'uritemplate/bin/uritemplate';
var isEnabled = false;
export default {
    enableApi: function () {
        isEnabled = true;
    },
    disableApi: function () {
        isEnabled = false;
    },
    isApiEnabled: function () {
        return isEnabled;
    },
    buildUrl: function (urlTemplate, context) {
        if (urlTemplate[0] !== '/') {
            urlTemplate = '/' + urlTemplate;
        }
        return AJS.contextPath() + uritemplate.parse(urlTemplate).expand(context);
    },
    goToUrl: function (url) {
        window.location.href = url;
    },
    hasContext: function (context, fieldName) {
        if (!context[fieldName]) {
            window.AJS.error("Missing " + fieldName + " in navigator context");
            return false;
        }
        return true;
    },
    appendQueryParam: function (url, key, value) {
        return url + (url.indexOf('?') > -1 ? '&' : '?') + key + '=' + encodeURIComponent(value);
    }
};
