const babelify = require('babelify');
const browserify = require('browserify');
const envify = require('envify/custom');
const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const derequire = require('gulp-derequire');
const gutil = require('gulp-util');
const unreachableBranch = require('unreachable-branch-transform');
const buffer = require('vinyl-buffer');
const source = require('vinyl-source-stream');
const watchify = require('watchify');

function build(entryModule, distModule, options) {
    const bundler = browserify(entryModule, {
        debug: false,
        standalone: distModule,
        sourceMap: false
    })
        .transform(babelify.configure({sourceMap:false}))
        .transform(envify(options.env || {}))
        .transform(unreachableBranch)

    function rebundle(bundler) {
        return bundler.bundle()
            .on('error', function (err) {
                gutil.log(gutil.colors.red('Browserify error'), err.message);
                this.emit('end');
            })
            .pipe(source(distModule + '.js'))
            .pipe(buffer())
            .pipe(derequire())
            .pipe(gulp.dest('./dist'));
    }

    if (options.watch) {
        const watchBundler = watchify(bundler);
        watchBundler.on('update', function () {
            gutil.log('Rebundling', gutil.colors.blue(entryModule));
            rebundle(watchBundler);
        });
    }

    gutil.log('Bundling', gutil.colors.blue(entryModule));
    return rebundle(bundler);
}

function buildHost(options) {
    options = options || {};
    return build('./src/host/index.js', 'connect-host-request', {
        env: {ENV: 'host'},
        watch: options.watch
    });
}
const watchHost = buildHost.bind(null, {watch: true});

function buildPlugin() {
    return gulp.src('./src/plugin/index.js')
        .pipe(concat('connect-plugin-request.js'))
        .pipe(babel())
        .pipe(gulp.dest('./dist'));
}

gulp.task('host:build', buildHost);
gulp.task('host:watch', watchHost);

gulp.task('plugin:build', buildPlugin);

gulp.task('watch', watchHost);
gulp.task('build', gulp.series(['plugin:build', 'host:build']));

gulp.task('default', gulp.series(['host:build', 'plugin:build']));
