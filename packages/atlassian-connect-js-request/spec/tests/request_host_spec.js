import requestHost, { stripJWTUrls, addRequestMarshal, removeRequestMarshal} from '../../src/host';

describe('request', () => {
  let contextPath;

  beforeEach(function() {
    jasmine.Ajax.install();
    contextPath = '';
  });

  afterEach(function() {
    jasmine.Ajax.uninstall();
  });

  window.AJS.contextPath = () => contextPath;

  const defaultContext = {
    extension: {
      addon_key: 'my-add-on',
      key: 'my-add-on-module',
      options: {}
    }
  }

  describe('JWT URL removal', () => {

    it('removes the URL when a JWT token is present', () => {
      const data = {
        'id': '33028',
        'type': 'page',
        'status': 'current',
        'title': 'fsd Home',
        'body': {
          'view': {
            'value': '</div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n \"h\":\"\",\n    \"url\":\"http://xxxx.ngrok.io/macro-editor-test?tz=Europe%2FLondon&loc=en-GB&user_id=admin&user_key=ff80808160c1063e0160c1069f960000&xdm_e=http%3A%2F%2Fcwhittington%3A1990&xdm_c=channel-macro-editor-test__some-general-page3761732419708178145&cp=%2Fconfluence&xdm_deprecated_addon_key_do_not_use=macro-editor-test&lic=none&cv=2.0.0-SNAPSHOT&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZjgwODA4MTYwYzEwNjNlMDE2MGMxMDY5Zjk2MDAwMCIsInFzaCI6IjA0MTA4OTljYjM1NTUxNWMwZTNjZjNhMjA0ZGNjZGVmNzJjZmZiYzM2MDUyZWIyOGY5MzIzZTc0ZDBmOGI1YzQiLCJpc3MiOiJlNWQ5Yjg4Ny0wOWRiLTMxMDktODRjNy1mNWNiNDI5NDdjYjkiLCJjb250ZXh0Ijp7InVzZXIiOnsidXNlcktleSI6ImZmODA4MDgxNjBjMTA2M2UwMTYwYzEwNjlmOTYwMDAwIiwidXNlcm5hbWUiOiJhZG1pbiIsImRpc3BsYXlOYW1lIjoiQS4gRC4gTWluaXN0cmF0b3IifX0sImV4cCI6MTUxNTA2NzY5NiwiaWF0IjoxNTE1MDY3NTE2fQ.n5OLakau13XaK1Tw-4cBVKNaY6-TZct162dzG6soOYI\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"{\\\"page.id\\\":\\\"33020\\\",\\\"macro.id\\\":\\\"ad511853-0640-45b6-8079-740525a2ff33\\\"}\",\n    \"timeZone\":\"Europe/London\",\n    \"origin\":\"http://70884b44.ngrok.io\",\n    \"hostOrigin\":\"http://cwhittington:1990\"\n};\n    if(window.AP && window.AP.subCreate) {\n      window._AP.appendConnectAddon(data);\n    } else {\n      require([\'ac/create\'], function(create){\n        create.appendConnectAddon(data);\n      });\n    }\n  }());\n\n//]]>\n</script>\n</div>\n</p><p><div class=\"ap-container conf-macro output-block\" id=\"ap-macro-editor-test__some-general-page2408087298299708922\" data-hasbody=\"false\" data-macro-name=\"some-general-page\" data-macro-id=\"ad3dd5aa-9956-45d1-b825-ee5725e36708\">\n\n  <div class=\"ap-content\" id=\"embedded-macro-editor-test__some-general-page2408087298299708922\"> </div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n    \"addon_key\":\"macro-editor-test\",\n    \"uniqueKey\":\"macro-editor-test__some-general-page2408087298299708922\",\n    \"key\":\"some-general-page\",\n    \"cp\":\"/confluence\",\n    \"uid\":\"admin\",\n    \"ukey\":\"ff80808160c1063e0160c1069f960000\",\n    \"general\":\"\",\n    \"w\":\"\",\n    \"h\":\"\",\n    \"url\":\"http://xxxx.ngrok.io/macro-editor-test?tz=Europe%2FLondon&loc=en-GB&user_id=admin&user_key=ff80808160c1063e0160c1069f960000&xdm_e=http%3A%2F%2Fcwhittington%3A1990&xdm_c=channel-macro-editor-test__some-general-page2408087298299708922&cp=%2Fconfluence&xdm_deprecated_addon_key_do_not_use=macro-editor-test&lic=none&cv=2.0.0-SNAPSHOT&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZjgwODA4MTYwYzEwNjNlMDE2MGMxMDY5Zjk2MDAwMCIsInFzaCI6ImY5OWIyZjZjNzhlZjk1MjNkNmIxZDA5M2IwODAwNzI1YzcwOGEzYWFhY2E4NjNjNTFmNWNmNDFiMWFiZGQ2M2QiLCJpc3MiOiJlNWQ5Yjg4Ny0wOWRiLTMxMDktODRjNy1mNWNiNDI5NDdjYjkiLCJjb250ZXh0Ijp7InVzZXIiOnsidXNlcktleSI6ImZmODA4MDgxNjBjMTA2M2UwMTYwYzEwNjlmOTYwMDAwIiwidXNlcm5hbWUiOiJhZG1pbiIsImRpc3BsYXlOYW1lIjoiQS4gRC4gTWluaXN0cmF0b3IifX0sImV4cCI6MTUxNTA2NzY5NiwiaWF0IjoxNTE1MDY3NTE2fQ.xjkR608b110g1KthhZ7-ETEQnATxpcCAIE9CdGh43Fs\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"'
          }
        }
      };

      const cleanedData = {
        'id': '33028',
        'type': 'page',
        'status': 'current',
        'title': 'fsd Home',
        'body': {
          'view': {
            'value': '</div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n \"h\":\"\",\n    \"url\":\"\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"{\\\"page.id\\\":\\\"33020\\\",\\\"macro.id\\\":\\\"ad511853-0640-45b6-8079-740525a2ff33\\\"}\",\n    \"timeZone\":\"Europe/London\",\n    \"origin\":\"http://70884b44.ngrok.io\",\n    \"hostOrigin\":\"http://cwhittington:1990\"\n};\n    if(window.AP && window.AP.subCreate) {\n      window._AP.appendConnectAddon(data);\n    } else {\n      require([\'ac/create\'], function(create){\n        create.appendConnectAddon(data);\n      });\n    }\n  }());\n\n//]]>\n</script>\n</div>\n</p><p><div class=\"ap-container conf-macro output-block\" id=\"ap-macro-editor-test__some-general-page2408087298299708922\" data-hasbody=\"false\" data-macro-name=\"some-general-page\" data-macro-id=\"ad3dd5aa-9956-45d1-b825-ee5725e36708\">\n\n  <div class=\"ap-content\" id=\"embedded-macro-editor-test__some-general-page2408087298299708922\"> </div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n    \"addon_key\":\"macro-editor-test\",\n    \"uniqueKey\":\"macro-editor-test__some-general-page2408087298299708922\",\n    \"key\":\"some-general-page\",\n    \"cp\":\"/confluence\",\n    \"uid\":\"admin\",\n    \"ukey\":\"ff80808160c1063e0160c1069f960000\",\n    \"general\":\"\",\n    \"w\":\"\",\n    \"h\":\"\",\n    \"url\":\"\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"'
          }
        }
      };
      expect(stripJWTUrls(JSON.stringify(data))).toEqual(JSON.stringify(cleanedData));
    });

    it('does not remove URLs without JWT tokens', () => {
      const data = {
        'body': {
          'view': {
            'value': '</div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n \"h\":\"\",\n    \"url\":\"http://xxxx.ngrok.io/macro-editor-test?tz=Europe%2FLondon&loc=en-GB&user_id=admin&user_key=ff80808160c1063e0160c1069f960000&xdm_e=http%3A%2F%2Fcwhittington%3A1990&xdm_c=channel-macro-editor-test__some-general-page3761732419708178145&cp=%2Fconfluence&xdm_deprecated_addon_key_do_not_use=macro-editor-test&lic=none&cv=2.0.0-SNAPSHOT\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"{\\\"page.id\\\":\\\"33020\\\",\\\"macro.id\\\":\\\"ad511853-0640-45b6-8079-740525a2ff33\\\"}\",\n    \"timeZone\":\"Europe/London\",\n    \"origin\":\"http://70884b44.ngrok.io\",\n    \"hostOrigin\":\"http://cwhittington:1990\"\n};\n    if(window.AP && window.AP.subCreate) {\n      window._AP.appendConnectAddon(data);\n    } else {\n      require([\'ac/create\'], function(create){\n        create.appendConnectAddon(data);\n      });\n    }\n  }());\n\n//]]>\n</script>\n</div>\n</p><p><div class=\"ap-container conf-macro output-block\" id=\"ap-macro-editor-test__some-general-page2408087298299708922\" data-hasbody=\"false\" data-macro-name=\"some-general-page\" data-macro-id=\"ad3dd5aa-9956-45d1-b825-ee5725e36708\">\n\n  <div class=\"ap-content\" id=\"embedded-macro-editor-test__some-general-page2408087298299708922\"> </div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n    \"addon_key\":\"macro-editor-test\",\n    \"uniqueKey\":\"macro-editor-test__some-general-page2408087298299708922\",\n    \"key\":\"some-general-page\",\n    \"cp\":\"/confluence\",\n    \"uid\":\"admin\",\n    \"ukey\":\"ff80808160c1063e0160c1069f960000\",\n    \"general\":\"\",\n    \"w\":\"\",\n    \"h\":\"\",\n    \"url\":\"http://xxxx.ngrok.io/macro-editor-test?tz=Europe%2FLondon&loc=en-GB&user_id=admin&user_key=ff80808160c1063e0160c1069f960000&xdm_e=http%3A%2F%2Fcwhittington%3A1990&xdm_c=channel-macro-editor-test__some-general-page2408087298299708922&cp=%2Fconfluence&xdm_deprecated_addon_key_do_not_use=macro-editor-test&lic=none&cv=2.0.0-SNAPSHOT\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"'
          }
        }
      };
      expect(stripJWTUrls(JSON.stringify(data))).toEqual(JSON.stringify(data));
    });

    it('does not touch things which are not JSON', () => {
      const data = '</div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n \"h\":\"\",\n    \"url\":\"http://xxxx.ngrok.io/macro-editor-test?tz=Europe%2FLondon&loc=en-GB&user_id=admin&user_key=ff80808160c1063e0160c1069f960000&xdm_e=http%3A%2F%2Fcwhittington%3A1990&xdm_c=channel-macro-editor-test__some-general-page3761732419708178145&cp=%2Fconfluence&xdm_deprecated_addon_key_do_not_use=macro-editor-test&lic=none&cv=2.0.0-SNAPSHOT&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZjgwODA4MTYwYzEwNjNlMDE2MGMxMDY5Zjk2MDAwMCIsInFzaCI6IjA0MTA4OTljYjM1NTUxNWMwZTNjZjNhMjA0ZGNjZGVmNzJjZmZiYzM2MDUyZWIyOGY5MzIzZTc0ZDBmOGI1YzQiLCJpc3MiOiJlNWQ5Yjg4Ny0wOWRiLTMxMDktODRjNy1mNWNiNDI5NDdjYjkiLCJjb250ZXh0Ijp7InVzZXIiOnsidXNlcktleSI6ImZmODA4MDgxNjBjMTA2M2UwMTYwYzEwNjlmOTYwMDAwIiwidXNlcm5hbWUiOiJhZG1pbiIsImRpc3BsYXlOYW1lIjoiQS4gRC4gTWluaXN0cmF0b3IifX0sImV4cCI6MTUxNTA2NzY5NiwiaWF0IjoxNTE1MDY3NTE2fQ.n5OLakau13XaK1Tw-4cBVKNaY6-TZct162dzG6soOYI\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"{\\\"page.id\\\":\\\"33020\\\",\\\"macro.id\\\":\\\"ad511853-0640-45b6-8079-740525a2ff33\\\"}\",\n    \"timeZone\":\"Europe/London\",\n    \"origin\":\"http://70884b44.ngrok.io\",\n    \"hostOrigin\":\"http://cwhittington:1990\"\n};\n    if(window.AP && window.AP.subCreate) {\n      window._AP.appendConnectAddon(data);\n    } else {\n      require([\'ac/create\'], function(create){\n        create.appendConnectAddon(data);\n      });\n    }\n  }());\n\n//]]>\n</script>\n</div>\n</p><p><div class=\"ap-container conf-macro output-block\" id=\"ap-macro-editor-test__some-general-page2408087298299708922\" data-hasbody=\"false\" data-macro-name=\"some-general-page\" data-macro-id=\"ad3dd5aa-9956-45d1-b825-ee5725e36708\">\n\n  <div class=\"ap-content\" id=\"embedded-macro-editor-test__some-general-page2408087298299708922\"> </div>\n  <script class=\"ap-iframe-body-script\">//<![CDATA[\n\n  (function(){\n    var data = {\n    \"addon_key\":\"macro-editor-test\",\n    \"uniqueKey\":\"macro-editor-test__some-general-page2408087298299708922\",\n    \"key\":\"some-general-page\",\n    \"cp\":\"/confluence\",\n    \"uid\":\"admin\",\n    \"ukey\":\"ff80808160c1063e0160c1069f960000\",\n    \"general\":\"\",\n    \"w\":\"\",\n    \"h\":\"\",\n    \"url\":\"http://xxxx.ngrok.io/macro-editor-test?tz=Europe%2FLondon&loc=en-GB&user_id=admin&user_key=ff80808160c1063e0160c1069f960000&xdm_e=http%3A%2F%2Fcwhittington%3A1990&xdm_c=channel-macro-editor-test__some-general-page2408087298299708922&cp=%2Fconfluence&xdm_deprecated_addon_key_do_not_use=macro-editor-test&lic=none&cv=2.0.0-SNAPSHOT&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZjgwODA4MTYwYzEwNjNlMDE2MGMxMDY5Zjk2MDAwMCIsInFzaCI6ImY5OWIyZjZjNzhlZjk1MjNkNmIxZDA5M2IwODAwNzI1YzcwOGEzYWFhY2E4NjNjNTFmNWNmNDFiMWFiZGQ2M2QiLCJpc3MiOiJlNWQ5Yjg4Ny0wOWRiLTMxMDktODRjNy1mNWNiNDI5NDdjYjkiLCJjb250ZXh0Ijp7InVzZXIiOnsidXNlcktleSI6ImZmODA4MDgxNjBjMTA2M2UwMTYwYzEwNjlmOTYwMDAwIiwidXNlcm5hbWUiOiJhZG1pbiIsImRpc3BsYXlOYW1lIjoiQS4gRC4gTWluaXN0cmF0b3IifX0sImV4cCI6MTUxNTA2NzY5NiwiaWF0IjoxNTE1MDY3NTE2fQ.xjkR608b110g1KthhZ7-ETEQnATxpcCAIE9CdGh43Fs\",\n    \"contentClassifier\":\"content\",\n    \"productCtx\":\"';
      expect(stripJWTUrls(data)).toEqual(data);
    });

  });

  it('calls ajax with correct options', () => {
    const requestOptions = {
      url: '/some/url'
    }
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;

    jasmine.Ajax.stubRequest(/.*/).andReturn({
      'responseText': 'immediate response'
    });

    requestHost.request(requestOptions, callback);

    expect(jasmine.Ajax.requests.mostRecent().method).toBe('GET');
    expect(jasmine.Ajax.requests.mostRecent().params).toEqual(null);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['Accept']).toEqual('*/*');
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['AP-Client-Key']).toEqual(defaultContext.extension.addon_key);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['x-ecosystem-user-agent']).toEqual('connect');
    expect(jasmine.Ajax.requests.mostRecent().url).toContain(requestOptions.url);
  });

  it('appends data to url as query params. as per jquery', () => {
    const requestOptions = {
      url: '/some/url',
      data: {
        a: 'a data',
        b: 'more data'
      }
    }
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;

    jasmine.Ajax.stubRequest(/.*/).andReturn({
      'responseText': 'immediate response'
    });
    requestHost.request(requestOptions, callback);
    expect(jasmine.Ajax.requests.mostRecent().method).toBe('GET');
    expect(jasmine.Ajax.requests.mostRecent().params).toEqual(requestOptions.data);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['Accept']).toEqual('*/*');
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['AP-Client-Key']).toEqual(defaultContext.extension.addon_key);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['x-ecosystem-user-agent']).toEqual('connect');
    expect(jasmine.Ajax.requests.mostRecent().url).toContain('/some/url?a=a%20data&b=more%20data&_r=');
  });

  it('headers are properly whitelisted', () => {
    const ifMatchVal = 'if-match-value';
    const ifNoneMatchVal = 'if-none-match-value';
    const requestOptions = {
      url: '/some/url',
      headers: {
        'If-Match': ifMatchVal,
        'If-None-Match': ifNoneMatchVal,
        'Not-Valid-Header': 'does not matter'
      }
    }
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;

    jasmine.Ajax.stubRequest(/.*/).andReturn({
      'responseText': 'response text'
    });

    requestHost.request(requestOptions, callback);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['If-Match']).toEqual(ifMatchVal);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['If-None-Match']).toEqual(ifNoneMatchVal);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['Not-Valid-Header']).not.toBeDefined();
  });

  describe('experimental header', () => {
    it('is not set by default', () => {
      const requestOptions = {
        url: '/some/url'
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });

      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().requestHeaders['X-ExperimentalApi']).not.toBeDefined();
    });

    it('is set when experimental is true', () => {
      const requestOptions = {
        url: '/some/url',
        experimental: true
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });
      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().requestHeaders['X-ExperimentalApi']).toEqual('opt-in');
    });
  });

  describe('binary attachment', () => {
    it('is not set by default', () => {
      const requestOptions = {
        url: '/some/url'
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });

      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().responseType === 'text');
    });

    it('is set when binaryAttachment is true', () => {
      const requestOptions = {
        url: '/some/url',
        binaryAttachment: true
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });
      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().responseType === 'arraybuffer');
    });
  });


  describe('file upload', () => {
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;
    const file = new Blob(['some file for upload']);
    file.name = 'somename.txt';
    const comment = 'this is a comment';
    const requestOptions = {
      url: '/some/url',
      type: 'POST',
      contentType: 'multipart/form-data',
      data: {
        file: file,
        comment: comment
      }
    };

    it('adds the X-Atlassian-Token header to the request', () => {
      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });
      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().requestHeaders['X-Atlassian-Token']).toEqual('no-check');
    });

    it('constructs the FormData', () => {
      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });
      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().params instanceof FormData).toBe(true);
    });

    it('throws an error when data is not an object', () => {
      const badrequestOptions = {
        url: '/some/url',
        type: 'POST',
        contentType: 'multipart/form-data',
        data: 'test'
      };
      try {
        jasmine.Ajax.stubRequest(/.*/).andReturn({
          'responseText': 'response text'
        });
        requestHost.request(badrequestOptions, callback);
      } catch (e) {
        expect(e.message).toEqual('For a Multipart request, data must to be an Object');
      }
    });
  });

  describe('callbacks', () => {
    const requestOptions = {
      url: '/some/url'
    };

    it('done callback is called (JSON)', () => {
      const data = 'some data';
      const statusText = '200';

      const callback = jasmine.createSpy('callback').and.callFake((errorThrown, textStatus, cbdata) => {
        expect(errorThrown).toBe(false);
        expect(cbdata).toEqual(data);
        expect(textStatus).toBeDefined();
      });

      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': data,
        'status': statusText
      });

      requestHost.request(requestOptions, callback);
      expect(callback).toHaveBeenCalled();
    });

    it('fail callback is called (JSON)', () => {
      const error = 'some error';
      const statusText = '500';

      const callback = jasmine.createSpy('callback').and.callFake((errorThrown, textStatus, error2) => {
        expect(errorThrown).toEqual(error);
        expect(textStatus).toBeDefined();
        expect(error2).toEqual(error);
      });

      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': error,
        'status': statusText
      });
      requestHost.request(requestOptions, callback);
      expect(callback).toHaveBeenCalled();
    });

    it('Filters to only allowed response headers', () => {
      const data = 'some data';
      const statusText = '200';

      const callback = jasmine.createSpy('callback').and.callFake((errorThrown, textStatus, cbdata) => {
        expect(errorThrown).toBe(false);
        expect(cbdata).toEqual(data);
        expect(textStatus).toBeDefined();
      });

      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': data,
        'status': statusText,
        responseHeaders: {
          'Content-Type': 'content',
          'ETag': 'etag',
          'Link': 'link',
          'Retry-After': 'retry',
          'X-RateLimit-Reset': 'rate-limit-reset',
          'X-RateLimit-NearLimit': 'rate-limit-near',
          'Set-Cookie': 'should not be set',
        }
      });

      requestHost.request(requestOptions, callback);
      expect(callback).toHaveBeenCalledWith(false, {
        headers: {
          'Content-Type': 'content',
          ETag: 'etag',
          Link: 'link',
          'Retry-After': 'retry',
          'X-RateLimit-Reset': 'rate-limit-reset',
          'X-RateLimit-NearLimit': 'rate-limit-near',
        },
        status: '200',
        statusText: '',
        responseText: data,
      }, data);
    })
  });

  describe('request marshal', () => {
    afterEach(() => {
      removeRequestMarshal();
    });

    it('stops the ajax request on an exception', () => {
      const anError = 'Sorry, no ajax today';
      const requestOptions = {
        url: '/some/url/unique/not/used',
        cache: true
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;
      const marshalSpy = jasmine.createSpy('spy').and.throwError(anError);

      addRequestMarshal(marshalSpy);
      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'abc123'
      });
      requestHost.request(requestOptions, callback);
      expect(marshalSpy).toHaveBeenCalled();
      expect(callback).toHaveBeenCalled();
      // make sure the ajax call was never sent.
      expect(jasmine.Ajax.requests.mostRecent().url).toBe(requestOptions.url);
      expect(jasmine.Ajax.requests.mostRecent().readyState).toBe(0);


    });
    it('stops the ajax request if the marshal returns a string', () => {
      const aString = 'Sorry, no ajax today';
      const requestOptions = {
        url: '/some/url/unique/not/used2',
        cache: true
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;
      const marshalSpy = jasmine.createSpy('spy').and.returnValue(aString);
      addRequestMarshal(marshalSpy);
      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'abc123'
      });
      requestHost.request(requestOptions, callback);
      expect(marshalSpy).toHaveBeenCalled();
      expect(callback).toHaveBeenCalled();
      // make sure the ajax call was never sent.
      expect(jasmine.Ajax.requests.mostRecent().url).toBe(requestOptions.url);
      expect(jasmine.Ajax.requests.mostRecent().status).toEqual(0);
    });

    it('makes the request if marshal returns true', () => {
      const requestOptions = {
        url: '/some/url/unique/not/used3',
        cache: true
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;
      const marshalSpy = jasmine.createSpy('spy').and.returnValue(true);

      addRequestMarshal(marshalSpy);
      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'abc123'
      });
      requestHost.request(requestOptions, callback);
      expect(marshalSpy).toHaveBeenCalled();
      expect(callback).toHaveBeenCalled();

      expect(jasmine.Ajax.requests.mostRecent().url).toBe(requestOptions.url);
      // request was sent.
      expect(jasmine.Ajax.requests.mostRecent().status).toEqual(200);
    });
  });

  it('rejects if request path does not include context path', () => {
    contextPath = '/wiki';
    let callback = jasmine.createSpy('callback');
    callback._context = defaultContext;
    requestHost.request('/../foo', callback);
    expect(callback).toHaveBeenCalledWith(`Request url must include context path '${contextPath}'`, {});

    callback = jasmine.createSpy('callback');
    callback._context = defaultContext;
    requestHost.request('/%2e%2e/foo', callback);
    expect(callback).toHaveBeenCalledWith(`Request url must include context path '${contextPath}'`, {});

    expect(jasmine.Ajax.requests.count()).toBe(0);
  });

  it('allows request if addon is in allow list', () => {
    contextPath = '/wiki';
    let callback = jasmine.createSpy('callback');
    callback._context = defaultContext;
    callback._context.extension.options = { allowPathTraversal: true };
    let path = '/../foo';
    requestHost.request(path, callback);
    expect(jasmine.Ajax.requests.mostRecent().method).toBe('GET');
    expect(jasmine.Ajax.requests.mostRecent().params).toEqual(null);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['Accept']).toEqual('*/*');
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['AP-Client-Key']).toEqual(defaultContext.extension.addon_key);
    expect(jasmine.Ajax.requests.mostRecent().requestHeaders['x-ecosystem-user-agent']).toEqual('connect');
    expect(jasmine.Ajax.requests.mostRecent().url).toContain(path);
  });

  
  describe('Capability header', () => {

    it('is set', () => {

      const requestOptions = {
        url: '/some/url'
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      jasmine.Ajax.stubRequest(/.*/).andReturn({
        'responseText': 'response text'
      });
      requestHost.request(requestOptions, callback);
      expect(jasmine.Ajax.requests.mostRecent().requestHeaders['X-Atlassian-Capability']).toEqual('connect--my-add-on-module');
    });
  });

});