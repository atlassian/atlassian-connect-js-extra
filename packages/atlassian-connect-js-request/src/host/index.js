/**
* The Request Javascript module provides a mechanism for an add-on rendered in an iframe to make an XMLHttpRequest to the host product without requiring CORS.
*
* In contrast to REST calls made from the add-on server to the product directly, any requests made in the browser are evaluated in the context of the currently logged in user. The requested resource is still evaluated against the add-ons granted scopes.
*
* @name Request
* @module
* @example
* AP.request('/rest/api/latest/...', {
*   success: function(responseText){
*     alert(responseText);
*   }
* });
*/

const XHR_PROPERTIES = ['status', 'statusText', 'responseText'];
const XHR_PROPERTIESBINARY = ['status', 'statusText', 'response'];
const XHR_HEADERS = ['Content-Type', 'ETag', 'Link', 'X-RateLimit-NearLimit', 'X-RateLimit-Reset', 'Retry-After'];
const REQUEST_HEADERS_WHITELIST = [
  'If-Match',
  'If-None-Match',
  'X-Atlassian-Force-Account-ID',
  'X-Force-Accept-Language',
  'Accept-Language'
];

var requestMarshal;

function getBooleanFeatureFlag(flagName, defaultValue) {
  if (window.connectHost && window.connectHost.getBooleanFeatureFlag) {
    return window.connectHost.getBooleanFeatureFlag(flagName, true);
  }
  return defaultValue;
}

function getContextPath(){
  if(window.AJS && window.AJS.contextPath) {
    return window.AJS.contextPath();
  }
  var el = document.querySelector('meta[name=ajs-context-path]');
  if (el) {
    return el.getAttribute('content');
  }
  return '';
}

// strip URLs that contain JWT tokens tokens from a string of JSON.
function stripJWTUrls(str) {
/**
  search for http[s]
  then anything up to jwt= (but not including a ")
  then include the JWT and anything else up to the next \" (backslash and double quote.
  and replace with just the \" to keep the JSON valid
*/
  return str.replace(/(http[s]?:\/\/[^"]*?&jwt=[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*.*?)\\"/gi, '\\"');
}

// reduce the xhr object to the just bits we can/want to expose over the bridge
function toJSON(xhr, isBinary) {
  var json = {headers: {}};
  // only copy key properties and headers for transport across the bridge
  if (isBinary) {
    XHR_PROPERTIESBINARY.forEach(function (v, i) {
      json[v] = xhr[v];
      if(v === 'response'){
        try{
          // make sure it's valid JSON by parsing it.
          JSON.parse(xhr[v]);
          json[v] = stripJWTUrls(xhr[v]);
        } catch (e) {} // swallow, any non JSON responses to skip JWT string checking and continue on.
      }
    }, this);
  } else {
    XHR_PROPERTIES.forEach(function (v, i) {
      json[v] = xhr[v];
      if(v === 'responseText'){
        try{
          // make sure it's valid JSON by parsing it.
          JSON.parse(xhr[v]);
          json[v] = stripJWTUrls(xhr[v]);
        } catch (e) {} // swallow, any non JSON responses to skip JWT string checking and continue on.
      }
    }, this);
  }
  // only copy key response headers for transport across the bridge
  XHR_HEADERS.forEach(function (v, i) {
    json.headers[v] = xhr.getResponseHeader(v);
  }, this);

  return json;
}

function appendFormData(formData, key, value) {
  if (value._isBlob && value.blob && value.name) {
    formData.append(key, value.blob, value.name);
  } else {
    formData.append(key, value);
  }
  return formData;
}

function handleMultipartRequest (ajaxOptions) {
  ajaxOptions.contentType = false;
  ajaxOptions.processData = false;

  if (ajaxOptions.data && typeof ajaxOptions.data === 'object') {
    var formData = new FormData();
    Object.keys(ajaxOptions.data).forEach(function (key) {
      var formValue = ajaxOptions.data[key];
      if (Array.isArray(formValue)) {
        formValue.forEach(function (val, index) {
          formData = appendFormData(formData, `${key}[${index}]`, val);
        })
      } else {
        formData = appendFormData(formData, key, formValue);
      }
    });
    ajaxOptions.data = formData;
    ajaxOptions.headers['X-Atlassian-Token'] = 'no-check';
  } else {
    throw new Error('For a Multipart request, data must to be an Object');
  }
  return ajaxOptions;
}

/**
* allows for dynamic rejection of ajax requests before they can be invoked.
* eg: by checking against a whitelist
**/
function addRequestMarshal(marshal) {
  if(typeof marshal === 'function') {
    requestMarshal = marshal;
  } else {
    throw new Error('ACJS request marshal must be a function');
  }
}
function removeRequestMarshal() {
  requestMarshal = undefined;
}

export {stripJWTUrls};
export {addRequestMarshal, removeRequestMarshal};
export default {
  /**
  * Execute an XMLHttpRequest as a Promise, or via callbacks, in the context of the host application. The format of the response (dataType) will be set to "text" as default; however, if binary data is requested, it will be set to "arraybuffer".
  *
  * @param {String}         url                               Either the URI to request or an options object (as below) containing at least a 'url' property;<br />
  *                                                           This value should be relative to the context path of the host application.
  * @param {Object}         options                           The options of the request.
  * @param {String}         options.url                       The url to request from the host application, relative to the host's context path
  * @param {String}         [options.type=GET]                The HTTP method name.
  * @param {Boolean}        [options.cache=true]              If the request should be cached.
  * @param {String|Object}  [options.data]                    The body of the request; required if type is 'POST' or 'PUT'. Optionally, for 'GET' this will append the object as key=value pairs to the end of the URL query string.
  * @param {String}         [options.contentType]             The content-type string value of the entity body, above; required when data is supplied.
  * @param {Object}         [options.headers]                 An object containing headers to set; supported headers are: 'Accept', 'If-Match' and 'If-None-Match'.
  * @param {Function}       [options.success]                 An optional callback function executed on a 200 success status code.
  * @param {Function}       [options.error]                   An optional callback function executed when a HTTP status error code is returned.
  * @param {Boolean}        [options.experimental=false]      If this is set to true, the developer acknowledges that the API endpoint which is being called may be in beta state, and thus may also have a shorter deprecation cycle than stable APIs.
  * @param {Boolean}        [options.binaryAttachment=false]  If this is set to true, the developer is specifying a request for an attachment consisting of binary data (e.g. an image) and the format of the response will be set to "arraybuffer".
  * @example
  * // A simple POST request which logs response in the console.
  * AP.request({
  *   url: '/rest/api/latest/...',
  *   type: 'POST',
  *   data: {name: 'some text', description: 'test'},
  *   success: function(responseText){
  *     console.log(responseText);
  *   },
  *   error: function(xhr, statusText, errorThrown){
  *     console.log(arguments);
  *   }
  * });

  * @example
  * // Upload an attachment to a Confluence entity.
  * var fileToUpload = document.getElementById("fileInput").files[0];
  *
  * AP.request({
  *   url: '/rest/api/content/123456/child/attachment',
  *   type: 'POST',
  *   contentType: 'multipart/form-data',
  *   data: {comment: 'example comment', file: fileToUpload},
  *   success: function(responseText){
  *     alert(responseText);
  *   }
  * });

  * @example
  * // Get the current user info using a Promise
  *
  * AP.request('/rest/api/user/current')
  *   .then(data => alert(data.body))
  *   .catch(e => alert(e.err));
  */
  request: function(args, callback){
    var contextPath = getContextPath();
    var headers = {};

    callback = arguments[arguments.length - 1];
    const {
      addon_key: addonKey,
      key: moduleKey,
      options: { allowPathTraversal }
    } = callback._context.extension;
    if(typeof args === 'string') {
      args = { url: args };
    }

    var url = contextPath + args.url;
    if (!allowPathTraversal) {
      var fullUrl = new URL(url, document.baseURI);
      if (!fullUrl.pathname.startsWith(contextPath)) {
        if (window.connectHost && window.connectHost.trackAnalyticsEvent) {
          window.connectHost.trackAnalyticsEvent('jsapi.request.prevent-path-traversal', {
            addonKey,
            moduleKey,
            url
          });
        }
        return callback(`Request url must include context path '${contextPath}'`, {});
      }
    }

    args.headers = args.headers || {}
    Object.getOwnPropertyNames(args.headers).forEach(function(k) {
      headers[k.toLowerCase()] = args.headers[k];
    }, this);

    function done(data, textStatus, xhr) {
      callback(false, toJSON(xhr, isBinaryData), data);
    }
    function fail(xhr, textStatus, errorThrown) {
      callback(errorThrown, toJSON(xhr, isBinaryData), errorThrown);
    }

    // execute the request with our restricted set of inputs
    var ajaxOptions = {
      url: url,
      type: args.type || 'GET',
      data: args.data,
      dataType: 'text', // prevent jquery from parsing the response body
      contentType: args.contentType,
      cache: (typeof args.cache !== 'undefined') ? !!args.cache : undefined,
      headers: {
        // */* will undo the effect on the accept header of having set dataType to 'text'
        'Accept': headers.accept || '*/*',
        // send the client key header to force scope checks
        'AP-Client-Key': addonKey,
        // header for distinguishing in-chrome API access
        // https://hello.atlassian.net/wiki/spaces/~jcreenaune/pages/4896102459/Differentiating+between+in-chrome+API+access+vs+external
        'x-ecosystem-user-agent': 'connect',
      }
    };
    
    var capabilityHeader = 'connect--'.concat(moduleKey);
    // send the 'X-Atlassian-Capability' header to better attribute app requests
    ajaxOptions.headers['X-Atlassian-Capability'] = capabilityHeader;
    
    // if it's a multipart request, then transform data into a FormData object
    if (ajaxOptions.contentType === 'multipart/form-data') {
      ajaxOptions = handleMultipartRequest(ajaxOptions);
      // if it's not form data, append it to the URL, matching jquery's encoding
    }

    if (ajaxOptions.data && typeof ajaxOptions.data === 'object' && ajaxOptions.type.toUpperCase() === 'GET') {
      Object.keys(ajaxOptions.data).forEach(function (key) {
        ajaxOptions.url += (ajaxOptions.url.indexOf('?') >= 0 ? '&' : '?') + encodeURIComponent(key) + '=' + encodeURIComponent(ajaxOptions.data[key]);
      });
    }

    // set the experimental header
    if (args.experimental === true) {
      ajaxOptions.headers['X-ExperimentalApi'] = 'opt-in';
    }

    REQUEST_HEADERS_WHITELIST.forEach(function (header, index) {
      if (headers[header.toLowerCase()]) {
        ajaxOptions.headers[header] = headers[header.toLowerCase()];
      }
    }, this);

    if(!ajaxOptions.cache) {
      ajaxOptions.url += (ajaxOptions.url.indexOf('?') >= 0 ? '&' : '?') + '_r=' + new Date().getTime();
    }

    var request = new XMLHttpRequest();
    request.open(ajaxOptions.type, ajaxOptions.url, true);

    // set the responseType
    var isBinaryData = false;
    if (args.binaryAttachment) {
      isBinaryData = true;
      request.responseType = "arraybuffer";
    }

    // set headers
    if(ajaxOptions.contentType) {
      ajaxOptions.headers['Content-type'] = ajaxOptions.contentType;
    }
    Object.getOwnPropertyNames(ajaxOptions.headers).forEach((headerName) => {
      request.setRequestHeader(headerName, ajaxOptions.headers[headerName]);
    });
    

    // response loaded
    request.onload = function() {
      if (this.status >= 200 && this.status < 300) {
        // Success!
        if (isBinaryData) {
          done(request.response, request.statusText, request);
        } else {
          done(request.responseText, request.statusText, request);
        }
      } else {
        // We reached our target server, but it returned an error
        if (isBinaryData) {
          fail(request, request.statusText, request.response);
        } else {
          fail(request, request.statusText, request.responseText);
        }
      }
    };

    // response failed to load
    request.onerror = function() {
      // There was a connection error of some sort
      if (isBinaryData) {
        fail(request, request.statusText, request.response);
      } else {
        fail(request, request.statusText, request.responseText);
      }
    };

    try {
      // before making the request, see if there's a marital and delegate.
      var marshalResponse = true;
      if(requestMarshal) {
        marshalResponse = requestMarshal.call(null, ajaxOptions);
      }

      if(marshalResponse === true) {
        request.send(ajaxOptions.data || null);
      } else {
        request.abort();
        fail(
          request,
          typeof marshalResponse === 'string' ? marshalResponse : undefined,
          marshalResponse
        );
      }
    } catch ( e ) {
      request.abort();
      fail(request, undefined, e);
      console.error('ACJS Request: ', e.message, e);
    }
  }
};
