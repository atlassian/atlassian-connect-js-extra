// {}, function => new
// {url, success} => old
// url, {} => old
// url, function => new

(function(){
  if(!AP._hostModules || !AP._hostModules._globals || !AP._hostModules._globals.request) {
    return;
  }

  try {
    new File([], '');
    } catch (e) {
    File = function (data, name, options) {
      var newFile = new Blob(data, options);
      newFile.name = name;
      newFile.lastModifiedDate = new Date();
      return newFile;
    }
  }

// universal iterator utility
  function each(o, it) {
      var l;
      var k;
      if (o) {
          l = o.length;
          if (l != null && typeof o !== 'function') {
              k = 0;
              while (k < l) {
                  if (it.call(o[k], k, o[k]) === false) break;
                  k += 1;
              }
          } else {
              for (k in o) {
                  if (o.hasOwnProperty(k)) {
                      if (it.call(o[k], k, o[k]) === false) break;
                  }
              }
          }
      }
  }

  function extend (dest) {
      var args = arguments;
      var srcs = [].slice.call(args, 1, args.length);
      each(srcs, function (i, src) {
          each(src, function (k, v) {
              dest[k] = v;
          });
      });
      return dest;
  }

  // wrap blobs into an object so that it's extra properties can make it through the xdm bridge
  function wrapBlob(value) {
    if (value instanceof Blob && value.name) {
      return {blob: value, name: value.name, _isBlob: true};
    }
    return value;
  }

  var original_request = AP._hostModules._globals.request.bind({});

  function Xhr(data) {
    var xhr = extend({}, data);
    var headers = data.headers || {};
    delete xhr.headers;
    return extend(xhr, {
      getResponseHeader: function(key){
        var value = null;
        if (key) {
          key = key.toLowerCase();
          each(headers, function (k, v) {
            if (k.toLowerCase() === key) {
              value = v;
              return false;
            }
          });
        }
        return value;
      },
      getAllResponseHeaders: function(){
        var str = "";
        each(headers, function (k, v) {
          // prepend crlf if not the first line
           str += (str ? "\r\n" : "") + k + ": " + v;
        });
        return str;
      }
    });
  }

  function request(options, callback){
    return new AP._promise((resolve, reject) => {
      if (typeof options === "string") {
        if(typeof callback === "object") {
          callback.url = options;
          options = callback;
        } else {
          options = {url: options};
        }
      }

      // find callbacks in old syntax
      if(options.success){
        var success = options.success;
        delete options.success;
        var error = options.error || function(){};
        delete options.error;
        callback = function(err, response, body){
          if (err === false) {
            success(body, "success", Xhr(response));
          } else {
            error(Xhr(response), "error", err);
          }
        };
      } else if (typeof callback !== "function"){
        callback = function(err, response, body) {
          if (err === false) {
            resolve({body, xhr: Xhr(response)});
          } else {
            reject({xhr: Xhr(response), err})
          }
        }
      }

      if (options.contentType === 'multipart/form-data') {
        Object.keys(options.data).forEach(function(key) {
          var item = options.data[key];
          if (Array.isArray(item)) {
            item.forEach(function (val, index) {
              options.data[key][index] = wrapBlob(val);
            })
          } else {
            options.data[key] = wrapBlob(item);
          }
        })
      }

      original_request(options, callback);
    });
  }

  AP._hostModules._globals.request = request;
  AP.request = request;

}());
