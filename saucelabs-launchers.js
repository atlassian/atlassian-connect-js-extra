module.exports = {
  // Chrome latest
  chrome_latest_windows: {
      base: 'SauceLabs',
      browserName: 'chrome',
      platform: 'Windows 10'
  },
  chrome_latest_osx: {
      base: 'SauceLabs',
      browserName: 'chrome',
      platform: 'OS X 10.14'
  },

  // Edge latest
  edge_latest_windows: {
      base: 'SauceLabs',
      browserName: 'microsoftedge',
      platform: 'Windows 10'
  },
      edge_latest_osx: {
      base: 'SauceLabs',
      browserName: 'microsoftedge',
      platform: 'OS X 10.14'
  },

  // Safari latest
  safari_latest_osx: {
      base: 'SauceLabs',
      browserName: 'safari',
      platform: 'OS X 10.14'
  }
};
