# Running tests

## Unit tests

```shell
npm run karma
```

## Integration tests

Warning - these are very outdated and cannot be run currently.

### Jira

```shell
PRODUCT=jira gulp nightwatch
```

### Confluence

```shell
PRODUCT=confluence gulp nightwatch
```

# Releasing changes

1. Run `npm run build` in the package directory to build the changes to the package.
2. Run `npm version <minor|patch> --no-git-tag-version` in the package directory to update the package.json
3. Raise a PR to merge the changes to `master`. Once the PR is merged, the package will be automatically published to the public NPM registry.

Once you've published the changes, the newly published version of the package has to be installed in the products that consume it.

Follow the instructions at [HOW-TO Release ACJS for products - Releasing changes for atlassian-connect-js-extra packages](https://hello.atlassian.net/wiki/spaces/ECO/pages/1473343850/HOW-TO+Release+ACJS+for+products#Releasing-changes-for-atlassian-connect-js-extra-packages) for details.
