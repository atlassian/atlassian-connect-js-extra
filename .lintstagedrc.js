module.exports = {
  'packages/**/*.{js,json}': () => [
    'npm run build',
    'git add packages/*/dist/**'
  ]
}
