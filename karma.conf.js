var _ = require('lodash');
var envify = require('envify/custom');
var webpack = require('webpack');
var path = require('path');
var fs = require('graceful-fs');
var argv = require('yargs').argv;
var cwd = process.cwd();
var getDirs = (srcpath) => fs.readdirSync(srcpath).filter((file) => fs.statSync(path.join(srcpath, file)).isDirectory());

const PACKAGES = [
  'cookie', 'history', 'request', 'navigator'
];

const package = argv.package;
const bases = [];

if (package) {
  if(!_.includes(PACKAGES, package)) {
    // invalid, error
  }
  bases.push(cwd + '/packages/atlassian-connect-js-' + package);
} else {
  // run all
  PACKAGES.forEach((p) => {
    bases.push(cwd + '/packages/atlassian-connect-js-' + p);
  })
}

module.exports = function(config) {
  var baseConfig = require('./karma.base.conf.js')(config);

  bases.forEach((base) => {
    baseConfig.files.push(base + '/spec/tests/**/*.js');
    baseConfig.preprocessors[base + '/spec/tests/**/*.js'] = ['webpack'];
    baseConfig.preprocessors[base + '/src/**/*.js'] = ['webpack'];
    getDirs(base).forEach((root) => baseConfig.webpack.resolve.alias[root] = `${base}/${root}`);
  });

  baseConfig.webpack.plugins.push(new webpack.DefinePlugin({'process.env.ENV': '"host"'}));  

  config.set(baseConfig);
};
